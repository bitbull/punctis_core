<?php
namespace Punctis\Core;

class Gateway
{
    protected $url;

    protected $brandcode;

    protected $authkey;

    /** @var  LoggerInterface */
    protected $logger;

    function __construct($url, $brandcode, $authkey, LoggerInterface $logger)
    {
        $this->url = $url;
        $this->brandcode = $brandcode;
        $this->authkey = $authkey;
        $this->logger = $logger;
    }

    public function call($data = array())
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->url);
        $this->logger->debug(array("endpoint_url" => $this->url));

        $data['brandcode'] = $this->brandcode;
        $data['authkey'] = $this->authkey;
        $curldata = json_encode($data);
        $this->logger->debug(array('curldata' => $curldata));

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curldata))
        );
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curldata);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        try {
            $result = json_decode($result, true);
            $this->logger->debug(array('result' => $result));
        } catch (\Exception $e) {
            $result = array(
                'code' => 520, // 'Unknown Error' code
                'response' => null,
                "error" => array("code" => $e->getCode(), "description" => $e->getMessage())
            );
            $this->logger->err(array('result' => $result));
        }
        curl_close($curl);
        return $result;
    }
}