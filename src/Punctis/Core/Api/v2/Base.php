<?php
namespace Punctis\Core\Api\v2;

/**
 * Base class inherited by other Api classes
 *
 * @package Punctis\Core\Api\v2
 */
abstract class Base
{
    /** @var Gateway */
    protected $gateway;

    function __construct($gateway)
    {
        $this->gateway = $gateway;
    }
}
