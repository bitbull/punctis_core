<?php
namespace Punctis\Core\Api\v2;
use Punctis\Core\Gateway as Gateway;

/**
 * Class User
 * @package Punctis\Core\Api\v2
 * @todo complete class implementation
 */
class User extends Base
{
    /**
     * Register an new user to the Engagement Program
     */
    public function setUser(array $arguments, array $user, array $legalDocuments = array())
    {
        $data = array(
            'command' => 'setUser',
            'arguments' => $arguments,
            'user' => $user,
        );
        // In case of empty legal optins, don't pass the 'legals' parameter to avoid API error
        if (!empty($legalDocuments)) {
            $data['legals'] = $legalDocuments;
        }

        return $this->gateway->call($data);
    }

    /**
     * Get the information about the Member-gets-Members activity of a specific user.
     */
    public function getUsersRef(array $identity)
    {
        $data = array(
            'command' => 'getUsersRef',
            'arguments' => (object) null, // 'arguments' parameter is mandatory even if empty
            'identity' => $identity,
        );
        return $this->gateway->call($data);
    }

    /**
     * Get the list of the Surprises the user gained due to its activity.
     * For each surprise, provides infos and instructions to use the gift.
     */
    public function getUserInstantRewards(array $identity)
    {
        $data = array(
            'command' => 'getUserInstantRewards',
            'arguments' => (object) null, // 'arguments' parameter is mandatory even if empty
            'identity' => $identity,
        );
        return $this->gateway->call($data);
    }
}