<?php
namespace Punctis\Core\Api\v2;

/**
 * Class PointCollection
 * @package Punctis\Core\Api\v2
 * @todo implement this class
 */
class PointCollection extends Base
{
    /**
     * Replies with the list of Actions included in your Point Collection.
     */
    public function getPcActions($arguments)
    {
        $data = array(
            'command' => 'getPcActions',
            'arguments' => $arguments,
        );
        return $this->gateway->call($data);
    }
}