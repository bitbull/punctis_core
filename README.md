# Punctis Core Library
Punctis base library integration.

## Release notes
The [semantic versioning v 2.0.0](http://semver.org/) convention is used.

Note: you can use the **{break}.{feature}.{fix}** formula to easily remember which number has to be changed after some
code changes.

### v 0.4.0 (release tag: v0.4.0-alpha)
* Add getUserInstantRewards method of User API

### v 0.3.0 (release tag: v0.3.0-alpha)
* Add getPcActions method of Point Collection API

### v 0.2.5 (release tag: v0.2.5-alpha)
* Add proper CURL Header
* Fix some empty/null parameters

### v 0.2.4
* Add endpoint URL logging

### v 0.2.3
* Remove CURL verbosity

### v 0.2.2
* Fix CURL parameters serialization

### v 0.2.1
* Inject legals
* Change command arguments order

### v 0.2.0
* Remove unused classes
* Add LoggerInterface
* Fix some wrong method calls

### v 0.1.0
* First commit
